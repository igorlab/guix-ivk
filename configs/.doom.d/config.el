;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Igor Korobov"
      user-mail-address "igor.korobov.gh@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
(setq doom-font (font-spec :family "UbuntuMono Nerd Font Mono" :size 24 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-gruvbox-light)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/tmp/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.


;; (doom-load-envvars-file "~/.emacs.d/.local/env")

(setq auto-mode-alist
      (append
       '(("\\.teh$"  . text-mode)
         ("\\.md$"   . text-mode)
         ("\\.adoc$" . text-mode)
         ("\\.tv5$" . text-mode)
         ("\\.tvv$" . text-mode)
         ("\\.pyx$"  . python-mode)
         ("\\.http$" . restclient-mode)
         ) auto-mode-alist))


(defun my-abbrevs ()
  (abbrev-mode)
  (define-abbrev text-mode-abbrev-table "i" "I")
  (define-abbrev text-mode-abbrev-table "i'm" "I'm")
  (define-abbrev text-mode-abbrev-table "i've" "I've")
  (define-abbrev text-mode-abbrev-table "i'd" "I'd")
  (define-abbrev text-mode-abbrev-table "i'll" "I'll"))


(add-hook 'text-mode-hook 'my-abbrevs)
(add-hook 'text-mode-hook '(lambda () (setq comment-start "//")))
(add-hook 'fundamental-mode-hook 'my-abbrevs)
(add-hook 'recentf-mode-hook #'(lambda () (setq recentf-max-saved-items 9999)))
(setq comment-line-break-function nil)


(add-to-list 'load-path "/home/ivk/dev/ivk-emacs-config")
(use-package! ivk)
(after! ivk
  (map!
   :g "<f4>" #'ivk.tmux/new-shell
   :g "<f5>" #'ivk.tmux/new-lf
   :g "<f6>" #'kill-this-buffer
   :g "C-M-g" #'(lambda () (interactive) (insert-register ?g))
   :g "C-(" #'ivk/enclose-in-parenthesis-and-enter-insert-mode
   :g "C-}" #'ivk/enclose-in-brackets-and-enter-insert-mode
   :g "C-{" #'ivk/enclose-in-braces-and-enter-insert-mode
   :i "M-h" #'backward-char
   :i "M-l" #'forward-char
   :i "M-t" #'counsel-company
   :leader
   :g "SPC" #'counsel-M-x
   :g "TAB" #'ivk/switch-to-previous-buffer
   :g "bb" #'ivy-switch-buffer
   :g "fs" #'ivk/save-buffer
   :g "fy" #'ivk/copy-buffer-file-name
   :g "fY" #'+default/yank-buffer-filename
   :g "cl" #'ivk/comment-or-uncomment-region-or-line
   :g "cr" #'symex-mode-interface
   :g "ti" #'ispell
   :g "td" #'magit-diff
   :g "tg" #'ivk.git/show-lazygit
   :g "rr" #'(lambda () (interactive) (save-buffer) (+eval-buffer)))
  (map!
   :map text-mode-map
   :g "M-r" #'ivk.notes/insert-id
   :g "M-R" #'ivk.notes/insert-id--sorted-by-date)
  ;; Fringe-mode is broken in Doom Emacs
  ;; (fringe-mode '(16 . 4))
  (add-hook 'emacs-startup-hook #'ivk.startup/show-greetings-screen))

;; This is a hack around Emacs' inability to distinguish C-i from TAB:
;; https://stackoverflow.com/questions/1792326/how-do-i-bind-a-command-to-c-i-without-changing-tab
(define-key input-decode-map (kbd "C-i") (kbd "H-i"))
(global-set-key (kbd "H-i") 'ivk/insert-space-and-enter-insert-mode)


(define-key evil-normal-state-map (kbd "ga") 'cider-eval-defun-at-point)
(define-key evil-normal-state-map (kbd "go") 'cider-eval-list-at-point)
(define-key evil-normal-state-map (kbd "ge") 'cider-eval-last-sexp)

(define-key evil-normal-state-map (kbd "g)") 'ivk/open-url-in-qutebrowser)

(after! evil
  (evil-select-search-module 'evil-search-module 'isearch)
  ;; Remove all the idiocy:
  (setq +evil-want-o/O-to-continue-comments nil)
  (setq evil-collection-setup-debugger-keys nil)
  (setq confirm-kill-emacs nil)
  (remove-hook 'doom-first-buffer-hook #'smartparens-global-mode))


(after! evil-escape
  (evil-escape-mode)
  (setq-default evil-escape-key-sequence ",."))


(setq fill-column 72)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Paredit & smartparens

(defun ivk/smartparens-mode ()
  (smartparens-mode)
  (turn-on-smartparens-strict-mode) ;; unsure...
  )


(defun ivk/paredit-mode-on-and-off ()
  (paredit-mode)
  (disable-paredit-mode))



(after! smartparens
  (require 'smartparens-config)
  (add-hook 'clojure-mode-hook #'ivk/smartparens-mode)
  (add-hook 'clojure-mode-hook #'ivk/paredit-mode-on-and-off)
  (add-hook 'emacs-lisp-mode-hook #'ivk/smartparens-mode))


(use-package symex
  :config
  (setq symex--user-evil-keyspec
        '(("." . symex-capture-forward)
          ("=" . ivk.clojure/zprint-function)))
  (symex-initialize)
  ;; obsolete
  (global-set-key (kbd "M-s") 'symex-mode-interface))


;; (setq symex-modal-backend 'hydra)
;; (setq symex-modal-backend 'evil)

;; (map! :map clojure-mode-map
;;       :g "M-s" 'hydra-paredit/body
;;       :map emacs-lisp-mode-map
;;       :g "M-s" 'hydra-paredit/body)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cider

(after! cider
  (map! :map cider-mode-map
        :leader
        :n "mc" 'ivk.clojure/clear-repl-buffer)
  (map! :map cider-mode-map
        :g "M-c" 'ivk.clojure/open-and-switch-to-or-out-of-repl-window
        :map cider-repl-mode-map
        :g "M-c" 'ivk.clojure/open-and-switch-to-or-out-of-repl-window))

;; Really dubious stuff:
(setq cider-clojure-cli-global-options "-A:dev")

(setq cider-show-error-buffer nil)

(add-to-list 'yas-snippet-dirs (ivk/locate-ivk-snippets))
(after! yasnippet
  ;; (add-to-list 'yas-snippet-dirs (ivk/locate-ivk-snippets))
  (add-hook 'text-mode-hook 'yas-minor-mode))


(after! ivy
  (setq ivy-height 20)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "(%d/%d) ")
  (ivy-set-display-transformer 'ivy-switch-buffer 'ivk.ivy/switch-buffer-transformer))


(after! flycheck
  (global-flycheck-mode))


(after! evil-snipe
  (setq evil-snipe-scope 'visible)
  (setq evil-snipe-override-local-mode-map
        (let ((map (make-sparse-keymap)))
          (evil-define-key* 'motion map
            "f" #'evil-snipe-f
            "F" #'evil-snipe-F
            "t" #'evil-snipe-t
            "T" #'evil-snipe-T)
          (when evil-snipe-override-evil-repeat-keys
            (evil-define-key* 'motion map
              "n" #'evil-snipe-repeat
              "N" #'evil-snipe-repeat-reverse))
          map))
  (setq evil-snipe-parent-transient-map
        (let ((map (make-sparse-keymap)))
          (define-key map "n" #'evil-snipe-repeat)
          (define-key map "N" #'evil-snipe-repeat-reverse)
          map))
  (define-key evil-snipe-parent-transient-map (kbd "C-;")
    (evilem-create 'evil-snipe-repeat
                   :bind ((evil-snipe-scope 'buffer)
                          (evil-snipe-enable-highlight)
                          (evil-snipe-enable-incremental-highlight)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Hydra

(defhydra hydra-paredit ()
  "Paredit"
  ("p" sp-forward-slurp-sexp "slurp-forward")
  ("(" ivk/insert-parenthesis-and-enter-insert-mode "insert ()")
  ("[" ivk/insert-brackets-and-enter-insert-mode "insert []")
  ("{" ivk/insert-braces-and-enter-insert-mode "insert {}")
  ("i" nil "DONE.")
  ;; ("SPC" (lambda () (interactive) (insert "ept") (normal-mode) (hydra-keyboard-quit) nil) "DONE.")
  ;; ("SPC" (lambda () (interactive) (insert "zu") (normal-mode) (hydra-disable)) "DONE.")
  ;; ...
  )




;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(ansi-color-names-vector
;;    ["#040408" "#9d0006" "#79740e" "#b57614" "#076678" "#b16286" "#427b58" "#282828"])
;;  '(custom-safe-themes
;;    (quote
;;     ("76bfa9318742342233d8b0b42e824130b3a50dcc732866ff8e47366aed69de11" "e2acbf379aa541e07373395b977a99c878c30f20c3761aac23e9223345526bcc" "fe94e2e42ccaa9714dd0f83a5aa1efeef819e22c5774115a9984293af609fce7" default)))
;;  '(fci-rule-color "#504945")
;;  '(jdee-db-active-breakpoint-face-colors (cons "#f0f0f0" "#a89984"))
;;  '(jdee-db-requested-breakpoint-face-colors (cons "#f0f0f0" "#79740e"))
;;  '(jdee-db-spec-breakpoint-face-colors (cons "#f0f0f0" "#928374"))
;;  '(objed-cursor-color "#9d0006")
;;  '(pdf-view-midnight-colors (cons "#282828" "#fbf1c7"))
;;  '(rustic-ansi-faces
;;    ["#fbf1c7" "#9d0006" "#79740e" "#b57614" "#076678" "#b16286" "#427b58" "#282828"])
;;  '(vc-annotate-background "#fbf1c7")
;;  '(vc-annotate-color-map
;;    (list
;;     (cons 20 "#79740e")
;;     (cons 40 "#8d7410")
;;     (cons 60 "#a17512")
;;     (cons 80 "#b57614")
;;     (cons 100 "#b3620e")
;;     (cons 120 "#b14e08")
;;     (cons 140 "#af3a03")
;;     (cons 160 "#af472e")
;;     (cons 180 "#b0545a")
;;     (cons 200 "#b16286")
;;     (cons 220 "#aa415b")
;;     (cons 240 "#a32030")
;;     (cons 260 "#9d0006")
;;     (cons 280 "#9a2021")
;;     (cons 300 "#97413c")
;;     (cons 320 "#946258")
;;     (cons 340 "#504945")
;;     (cons 360 "#504945")))
;;  '(vc-annotate-very-old-color nil))
;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  )

(put 'projectile-ripgrep 'disabled nil)
