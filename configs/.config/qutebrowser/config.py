config.load_autoconfig()

# config.set('url.searchengines', {"DEFAULT": "https://www.startpage.com/do/search/?q={}"})
config.set('url.searchengines', {"DEFAULT": "https://searx.be/search?q={}"})

config.set('hints.chars', 'aoeuhtnsidpyfgcrl')
config.set('tabs.background', True)
config.set('tabs.show', 'multiple')
config.set('zoom.default', 150)

config.bind(',l', 'navigate increment')
config.bind(',h', 'navigate decrement')

config.bind(',d', 'config-cycle content.user_stylesheets ~/tmp/solarized-everything-css/css/darculized/darculized-all-sites.css /home/ivk/tmp/solarized-everything-css/css/solarized-light/solarized-light-all-sites.css "" ;; reload')
config.bind(',m', 'hint links userscript download-magnet.bb.clj')
config.bind(',w', 'hint links userscript /home/ivk/.pyenv/shims/download-wallpaper-qb')
# config.bind(',c', 'spawn --userscript copy-colors.bb.clj')
config.bind(',c', 'spawn --userscript /home/ivk/dev/cfg/guix-ivk/scripts/copy-colors.bb.clj')

config.bind('yc', 'spawn bash -c "links -dump -width 64 {url} | xclip -selection clipboard"')



config.set('downloads.location.directory', '/storage/down/')
config.set('downloads.location.prompt', False)
