import os

config.load_autoconfig()

config.set('url.start_pages', ['file://' + os.environ.get('IVK_EMACSHELPER_HTML', 'whatever')])
config.set('url.searchengines', {"DEFAULT": "https://www.startpage.com/do/search/?q={}"})
config.set('hints.chars', 'aoeuhtnsidpyfgcrl')
config.set('tabs.background', True)
config.set('tabs.show', 'multiple')
config.set('zoom.default', 120)
config.set('new_instance_open_target', 'tab-silent')
config.set('content.user_stylesheets', '/home/ivk/tmp/solarized-everything-css/css/solarized-light/solarized-light-all-sites.css')

config.bind(',t', 'spawn notes locate --address {url} ;; spawn bspc node -f next.local')
config.bind(',.d', 'spawn bash -c "notes-make-link.bb.clj {url} \\"{title}\\" id | xclip -selection clipboard"')
config.bind(',.h', 'spawn bash -c "notes-make-link.bb.clj {url} \\"{title}\\" short | xclip -selection clipboard"')
config.bind(',.t', 'spawn bash -c "notes-make-link.bb.clj {url} \\"{title}\\" textless | xclip -selection clipboard"')
config.bind(',.n', 'spawn bash -c "notes-make-link.bb.clj {url} \\"{title}\\" full | xclip -selection clipboard"')
config.bind(',d', 'config-cycle content.user_stylesheets ~/tmp/solarized-everything-css/css/darculized/darculized-all-sites.css /home/ivk/tmp/solarized-everything-css/css/solarized-light/solarized-light-all-sites.css "" ;; reload')

config.set('downloads.location.directory', '/storage/down/')
config.set('downloads.location.prompt', False)
