#
# MOST BASIC
#

# terminal emulator
super + Return
	urxvt

# program launcher
super + d
	dmenu_run

# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd


#
# BASIC
#

# close and kill
super + {_,shift + }w
	bspc node -{c,k}

# set the window state
super + {apostrophe, q}
	bspc node -t {tiled,floating}

# focus the next/previous node in the current desktop
super + {_,shift + }n
	bspc-focus-ex.sh {next,prev}

# # Simpler version
# super + {_,shift + }n
# 	bspc node -f {next,prev}.local


# "important" desktops
super + f
	bspc desktop -f Start
super + g
	bspc desktop -f Em
super + c
	bspc desktop -f Bro
super + r
	bspc desktop -f Term
super + b
	bspc desktop -f Graph
super + a
	bspc desktop -f Audio


# "unimportant" desktops
super + h; super + l
	bspc desktop -f Comm
super + h; super + r
	bspc desktop -f Reader
super + h; super + n
	bspc desktop -f Notes
super + h; super + h
	bspc desktop -f Misc-I
super + h; super + b
	bspc desktop -f Misc-II
super + h; super + m
	bspc desktop -f Misc-III


super + shift + g
	bspc node focused --to-desktop Em
super + shift + c
	bspc node focused --to-desktop Bro
super + shift + r
	bspc node focused --to-desktop Term
super + shift + b
	bspc node focused --to-desktop Graph
super + shift + a
	bspc node focused --to-desktop Audio
super + shift + h; super + shift + h
	bspc node focused --to-desktop Misc-I
super + shift + h; super + shift + b
	bspc node focused --to-desktop Misc-II
super + shift + h; super + shift + m
	bspc node focused --to-desktop Misc-III


# move a floating window
super + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}


# alternate between the tiled and monocle layout
super + m
	bspc desktop -l next


# increase node's horizontal size
super + z
	bspc node -z left -20 0 ; bspc node -z right 20 0


#
# Apps and scripts
#

super + dollar
	notes lookup --jump

super + shift + dollar
	notes lookup --jump+

super + ampersand
	remote-play.bb.clj

super + bracketleft
	remote-stop.sh

XF86Calculator
	ydl-enqueue.bb.clj

super + ampersand
	remote-play.bb.clj

XF86AudioPlay
	systemctl suspend

#
# Background
#

super + control + Left
	~/bin/more/set-background.bb.clj -1

super + control + Down
	~/bin/more/set-background.bb.clj 0

super + control + Right
	~/bin/more/set-background.bb.clj 1
