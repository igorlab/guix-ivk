# Basic shit
set -U fish_features stderr-nocaret qmark-noglob
export IVKFISH="$HOME/dev/cfg/all-sorts-of-stuff/fish"

function fish_user_key_bindings
    bind -k f4 tmux-new-shell.sh
    bind -k f5 tmux-new-lf.sh
    bind -k f6 maybe-exit
    bind \ep up-or-search
    bind \en down-or-search
end

for src in (find "$IVKFISH/functions/" | grep "\.fish")
    source $src
end

ivk_startup
