#!/bin/bash
#
# Reaction on saving babashka file in Emacs.

file=$1

STDOUT_PATH="/dev/shm/_pythonhelper_stdout"
STDERR_PATH="/dev/shm/_pythonhelper_stderr"

ADOC_TEMPLATE='
{{stderr}}

== Stdout

----
{{stdout}}
----
'

AWK_SCRIPT='
{
    if ($0 == "{{stdout}}") {
	print STDOUT
    }
    else if ($0 == "{{stderr}}" && STDERR) {
	print("== Stderr")
	print("----")
	print STDERR
	print("----")
    }
    else if ($0 == "{{stderr}}") {
    }
    else {
	print $0
    }
}
'



bb "$file" "RUN!" \
    1> $STDOUT_PATH \
    2> $STDERR_PATH

success=$?
out=$(<$STDOUT_PATH)
err=$(<$STDERR_PATH)

# echo "" > "$IVK_EMACSHELPER_ADOC"
# if [ $success -ne 0 ]; then
#     # echo "Oblom!"
#     # exit $success
#     printf "== Stderr\n----\n%s\n----\n\n" err \
#            out >> "$IVK_EMACSHELPER_ADOC"
# fi

# Make the adoc.
printf "$ADOC_TEMPLATE" | awk "$AWK_SCRIPT" STDOUT="$out" STDERR="$err" \
    > "$IVK_EMACSHELPER_ADOC" \
    2> /tmp/_wtf_awk


# printf "== Stderr\n----\n%s\n----\n\n== Stdout\n----\n%s\n" \
#        "$err" "$out" > "$IVK_EMACSHELPER_ADOC"

asciidoctor "$IVK_EMACSHELPER_ADOC" --out-file="$IVK_EMACSHELPER_HTML" \
    2> /tmp/_wtf_asciidoctor

open-in-emacshelper.sh "$IVK_EMACSHELPER_HTML"
