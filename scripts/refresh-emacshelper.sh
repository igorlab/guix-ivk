#!/bin/bash
#
# Refresh emacshelper qutebrowser.

qb_version='1.8.1'
proto_version=1
socket_dir="$IVK_QB_EMACSHELPER"/runtime
ipc_socket=$socket_dir/$(ls -t $socket_dir | head -n 1 | xargs echo -e)

printf '{"args": [":reload"], "target_arg": null, "version": "%s", "protocol_version": %d, "cwd": "%s"}\n' \
       "${qb_version}" \
       "${proto_version}" \
       "${PWD}" | socat - UNIX-CONNECT:"${ipc_socket}"
