#!/usr/bin/env bash
set -euo pipefail

curl -X POST localhost:$IVK_NOTES_PORT/api/shutdown
