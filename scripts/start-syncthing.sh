#!/bin/bash
#
# Start syncthing in a 'services' tmux session.

tmux new-window -t services -n syncthing "syncthing -no-browser"
