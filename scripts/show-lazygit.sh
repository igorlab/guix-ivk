# DEPRECATED in favor of 'progs show-lazygit'

#!/bin/bash
#
# Show lazygit window.
set -euo pipefail
dir="$1"

darkness=${IVK_DARKNESS:-}
if [[ -n $darkness ]]; then
  name=URxvtGitDark
else
  name=URxvtGit
fi

urxvt -name $name \
      -cd "$dir" \
      -geometry 120x40 \
      -e lazygit
