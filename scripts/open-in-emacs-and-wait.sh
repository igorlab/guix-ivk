#!/bin/bash
#
# Open a file in Emacs and wait.

for var in "$@"
do
    emacsclient --eval "(find-file \"$var\")"
done

wmctrl -s 0
read -n 1 -s -r -p "Press any key when done..."
