#!/usr/bin/env bb
(ns tmux
  (:require [clojure.java.io :as io]
            [clojure.java.shell :as shell]))


(def temporary-directory "/tmp/_book")
(def widths [64 72 80])


(defn create-clean-temporary-directory []
  (shell/sh "rm" "-rf" temporary-directory)
  (shell/sh "mkdir" "-p" temporary-directory))


(defn convert-book [width]
  (:out
   (shell/sh "links" "-dump" "-width" (str width) (str temporary-directory "/" "index.html"))))


(defn unzip-book [path]
  (shell/sh "unzip" (.getCanonicalPath (io/file path)) :dir temporary-directory))


(defn main [path]
  (create-clean-temporary-directory)
  (unzip-book path)
  (doseq [width widths
          :let [path (str temporary-directory "/" "txt-" width ".txt")
                content (convert-book width)]]
    (spit path content)))


(do
  (assert (seq *command-line-args*))
  (main (nth *command-line-args* 0))
  (println (format "Book converted, go to '%s'" temporary-directory)))
