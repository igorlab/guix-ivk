#!/usr/bin/env bb

;; DEPRECATED

(ns notes-create-id
  (:require [babashka.curl :as curl]
            [cheshire.core :as json]
            [clojure.edn :as edn]
            [clojure.tools.cli :as cli]
            [clojure.string :as str]))


(def cli-options
  [[nil "--state-file FILE" "This file stores config (formely known as state) (required)"]
   [nil "--notes-file FILE" "Generate a note identifier for the file FILE (required)"]
   ["-h" "--help"]])


(assert (System/getenv "IVK_NOTES_PORT"))
(def port (Integer/parseInt (System/getenv "IVK_NOTES_PORT")))
(def address (format "http://localhost:%d" port))



(defn read-config [config-file]
  (-> config-file slurp edn/read-string))


(defn request-id [prefix suffix]
  (let [params (json/generate-string {:prefix prefix :suffix suffix})
        resp (curl/get (str address "/api/generate-id")
                       {:body params
                        :headers {"Accept" "application/json"
                                  "Content-Type" "application/json"}})]
    (-> resp :body)))


(defn create-id [config-file notes-file]
  (let [{:keys [prefix suffixes]} (read-config config-file)
        suffix (get suffixes (-> notes-file File. .getName) "")]
    (request-id prefix suffix)))


(defn main [args]
  (let [{:keys [options summary errors]} (cli/parse-opts args cli-options)
        {:keys [help state-file notes-file]} options]
    (cond help (println summary)
          errors (binding [*out* *err*] (run! println errors))
          (and state-file notes-file) (println (create-id state-file notes-file))
          :else (do (binding [*out* *err*] (println "Not all required keys are used!"))
                    (println summary)))))


(main *command-line-args*)
