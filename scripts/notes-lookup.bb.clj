#!/usr/bin/env bb

;; DEPRECATED

(ns notes-create-id
  (:require [babashka.curl :as curl]
            [cheshire.core :as json]
            [clojure.java.shell :as sh]
            [clojure.string :as str]
            [clojure.java.io :as io]))


(assert (System/getenv "IVK_NOTES_PORT"))
(assert (System/getenv "IVK_QB_EMACSHELPER"))
(assert (System/getenv "IVK_QB_NOTES"))

(def port (Integer/parseInt (System/getenv "IVK_NOTES_PORT")))
(def port-of-gras (Integer/parseInt (System/getenv "IVK_GRAS_PORT")))
(def address (format "http://localhost:%d" port))


(defn lookup [address-or-id]
  (let [params (json/generate-string {:address address-or-id})
        resp (curl/get (str address "/api/lookup")
                       {:body params
                        :headers {"Accept" "application/json"
                                  "Content-Type" "application/json"}})]
    (-> resp :body (json/parse-string true))))


(defn all-notes []
  (let [resp (curl/get (str address "/api/all-notes")
                       {:headers {"Accept" "application/json"}})]
    (-> resp :body (json/parse-string true) :notes)))


(defn bspwm-desktop []
  (->
   (sh/sh "bspc" "query" "--desktops" "-d" "--names")
   :out
   str/trim))


(defn bspwm-switch [desktop]
  (sh/sh "bspc" "desktop" "-f" desktop))


(defn qutebrowser-socket [qb-dir]
  (let [runtime-dir (io/file qb-dir "runtime")
        sockets (.listFiles runtime-dir)]
    (->> sockets
         (sort-by #(.lastModified %))
         last
         .getCanonicalPath)))


(defn qutebrowser-send [qb-dir cmd]
  (let [msg {:args [cmd]
             :target_arg nil
             :version "1.8.1"
             :protocol_version 1
             :cwd "."}
        msg-str (str (json/generate-string msg) "\n")
        socket (qutebrowser-socket qb-dir)]
    (sh/sh "socat" "-" (str "UNIX-CONNECT:" socket) :in msg-str)))


(defn open-in-emacs [file line]
  (sh/sh "emacsclient" "--eval"
         (format "(progn (find-file \"%s\") (goto-line %d) (recenter-top-bottom 5))"
                 file (inc line))))


(defn open-in-gras [id]
  (let [address (format "http://localhost:%d" port-of-gras)
        resp (curl/post (str address "/api/goto/" id))]
    (println (:body resp))))


(defn ask-choice [lines]
  (let [choices-file "/dev/shm/fzf-tmp-file"
        result-file "/dev/shm/fzf-tmp-result"
        fzf-command (format "cat '%s' | fzf > '%s'" choices-file result-file)
        _ (spit choices-file (str/join "\n" lines))
        _ (sh/sh "urxvt"
                 "-name" "URxvtChoices"
                 "-e" "bash" "-c" fzf-command)
        result (-> result-file slurp str/trim)]
    (when-not (empty? result)
      result)))


(defn ask-choice-index [lines]
  (let [len (count (str (count lines)))
        fmt (str "%-" len "d %s")
        choice (->> lines
                    (map (fn [n line] (format fmt n line)) (range))
                    ask-choice)]
    (when choice
      (->> choice (re-find #"^[0-9]+") Integer/parseInt))))


(defn lookup-note []
  (let [notes (all-notes)
        titles (map :dated-title notes)
        note-idx (ask-choice-index titles)]
    (when note-idx
      (-> (get notes note-idx) :tv5/id lookup))))


(defn main-jump [note]
  (let [{:tv5/keys [id origin-file origin-line]} note]
    (case (bspwm-desktop)
      "Em" (do
             (open-in-emacs origin-file origin-line)
             (qutebrowser-send (System/getenv "IVK_QB_EMACSHELPER")
                               (format ":open %s/note/%s" address id)))
      "Graph" (open-in-gras id)
      (do
        (qutebrowser-send (System/getenv "IVK_QB_NOTES")
                          (format ":open %s/note/%s" address id))
        (bspwm-switch "Notes")))))


(defn main-copy [note]
  (let [id (:tv5/id note)
        ref (format "{r/%s}" id)]
    (qutebrowser-send (System/getenv "IVK_QB_EMACSHELPER")
                      (format ":open %s/note/%s" address id))
    (sh/sh "xclip" "-selection" "clipboard" :in ref)))


(defn main [args]
  (let [action (or (first args) "--print")
        note (lookup-note)]
    (when note
      (case action
        "--print" (println (:tv5/id note))
        "--jump" (main-jump note)
        "--copy" (main-copy note)
        (println "Usage:\n  notes-lookup.bb.clj [--print | --jump | --copy]")))))


(main *command-line-args*)
nil
