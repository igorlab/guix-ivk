#!/bin/bash


bg=$(change-wallpaper.bb.clj --dir "$IVK_BG_OF_THE_DAY_SRC_DIR")
basename "$bg" > /tmp/bg-of-the-day.txt &

case $1 in
--cp)
     cp "$bg" "$IVK_BG_OF_THE_DAY_DEST_DIR"
     ;;
--ffu)
     mv "$bg" "$IVK_BG_OF_THE_DAY_FUTURE_DIR"
     bg-of-the-day.sh
     ;;
--rnd)
     seed=$RANDOM
     echo $seed
     echo $seed > /home/ivk/tmp/seed
     bg-of-the-day.sh
     ;;
--discard)
     mv "$bg" $IVK_BG_OF_THE_DAY_DISCARD_DIR
     bg-of-the-day.sh
     ;;
*)
     echo "$bg"
esac
