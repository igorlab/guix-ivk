#!/usr/bin/env bb
(ns tmux
  (:require [clojure.string :as str]
            [clojure.pprint :refer [pprint]]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.java.shell :as shell]))

;; Provides helper functions for tmux.
;; (Used in lfrc and, potentially, in other places.)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; General helpers

(defn index-of [coll key-fn]
  (loop [coll coll index 0]
    (let [[e & rst] coll]
      (cond (key-fn e) index
            (empty? rst) nil
            :else (recur rst (inc index))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tmux helpers


(defn window-exists? [session window]
  (->
   (shell/sh "tmux" "has-session" "-t" (str session ":" window))
   :exit
   (= 0)))


(defn current-session []
  (-> (shell/sh "tmux" "display-message" "-p" "#S") :out str/trim))


(defn remove-suffix [s suffix]
  (if (str/ends-with? s suffix)
    (subs s 0 (- (count s) (count suffix)))
    s))


(defn- parse-windows-line [line]
  (let [parsed (re-find #"^(\S+):(\d+): ([^\s*]+)([*]*) .*" line)
        [_ session number name status] parsed]
    (assert parsed)
    {:session session
     :number (Integer/parseInt number)
     :name (remove-suffix name "-")
     :active? (= status "*")}))


(defn list-windows []
  (->> (shell/sh "tmux" "list-windows" "-a")
       :out
       str/split-lines
       (mapv parse-windows-line)))


(defn next-window-after-kill
  "Return number of the window that should become active after the currently
  active window is killed."
  [session]
  (let [windows (->> (list-windows)
                     (filterv #(= (:session %) session)))
        active-window-index (index-of windows #(:active? %))
        max-window-index (dec (count windows))
        next-window-index (cond (= (count windows) 1) 0
                                (= active-window-index max-window-index) (dec active-window-index)
                                :else (inc active-window-index))]
    (:number (get windows next-window-index))))


(defn select-window [session window-number]
  (shell/sh "tmux" "select-window" "-t" (str session ":" window-number)))


;; Neisp
(defn set-last-window [session window-number]
  (shell/sh "tmux" "select-window" "-t" (str session ":" window-number)
            ";" "last-window"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main

(defn prepare-to-kill-window []
  (let [session (current-session)
        next-window (next-window-after-kill session)]
    (select-window session next-window)))


(defn main [args]
  (case (first args)
    "prepare-to-kill-window" (prepare-to-kill-window)
    ("--middleware" "RUN!") (println "Buffer reloaded")
    (throw (ex-info "Unknown command" {:command-line-arguments args}))))


(do
  (main *command-line-args*)
  nil)
