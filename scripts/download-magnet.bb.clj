#!/usr/bin/env bb
(ns ydl-enqueue
  (:require [clojure.string :as str]
            [clojure.java.shell :as shell]))


(assert (System/getenv "IVK_MAGNET_LINKS"))


(defn ensure-trailing-slash [dir-path]
  (str dir-path (when-not (str/ends-with? dir-path "/") "/")))


(def download-dir (ensure-trailing-slash (System/getenv "IVK_MAGNET_LINKS")))
(def icon-dir (ensure-trailing-slash (System/getenv "IVK_NOTIFY_ICON_DIR")))


(defn read-clipboard []
  (:out
   (shell/sh "xclip" "-o" "-selection" "clipboard")))


(defn report-error [msg]
  (binding [*out* *err*] (println msg))
  (shell/sh "notify-send"
            "-t" "3000"
            msg
            "-i" (str icon-dir "arctic-fox-simple.png")))


(defn valid-magnet-link? [magnet-link]
  (and (string? magnet-link)
       (re-matches #"magnet:\?xt=urn:btih:\w+.*" magnet-link)))


(defn magnet-to-torrent [magnet-link]
  (format "d10:magnet-uri%d:%se" (count magnet-link) magnet-link))


(defn torrent-file-name [magnet-link]
  (let [[_ hash] (re-find #".*urn:btih:(\w+).*" magnet-link)]
    (format "meta-%s.torrent" hash)))


(defn add-torrent [magnet-link]
  (if (valid-magnet-link? magnet-link)
    (spit (str download-dir (torrent-file-name magnet-link))
          (magnet-to-torrent magnet-link))
    (report-error (format "Not a magnet link: '%s'" magnet-link))))


(defn main [args]
  (case (first args)
    ("--xclip" nil) (add-torrent (read-clipboard))
    "--qb" (add-torrent (System/getenv "QUTE_URL"))
    (add-torrent (first args))))


(main *command-line-args*)
