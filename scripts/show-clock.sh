#!/bin/bash
#
# Run tty-clock in rxvt terminal.

prev_window=`xdotool search --classname URxvtClock`
xdotool windowkill $prev_window

urxvt -name URxvtClock \
      -geometry 40x20+297+1 \
      -fn "xft:pixelsize=15" \
      -title CLOCK \
      +sb \
      -e bash -c 'tty-clock -s -C 3'
