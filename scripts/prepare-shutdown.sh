#!/bin/bash
#
# Gently close certain processes as a preparation to shutdown.

shutdown-notes-server.sh
windows=$(ps -e | grep "\(mpv\|emacs\)" | awk '{print $1}')
for wnd in $windows
do
    kill -SIGTERM $wnd
done
