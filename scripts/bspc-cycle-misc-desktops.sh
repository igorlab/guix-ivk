#!/bin/bash
#
# Switch desktop between Misc-I and Misc-II.

curr=`bspc query -D -d focused --names`
if [ "$curr" = "Misc-I" ]; then
    next="Misc-II"
elif [ "$curr" = "Misc-II" ]; then
    next="Misc-I"
else
    next=`cat /tmp/tt-misc-desktops`
fi

if [ "$next" = "" ]; then
    next="Misc-I"
fi

echo $next > /tmp/tt-misc-desktops
bspc desktop -f $next
