#!/bin/bash
#
# Run tty-clock in rxvt terminal.

bspc rule -a \*:URxvtNedoclock sticky=True state=floating rectangle=860x160+10+910 border=off
urxvt -name URxvtNedoclock \
      -geometry 40x20+297+1 \
      -fn "xft:pixelsize=15" \
      -title CLOCK \
      +sb \
      -e bash -c 'tty-clock -s -C 3 -D'
