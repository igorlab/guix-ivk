#!/bin/bash

tmux select-window -t "lf:$" \
    ";" new-window -t "lf:+1" -n "$(basename $PWD)" lf \
    ";" switch -t lf:$

wmctrl -s 2
