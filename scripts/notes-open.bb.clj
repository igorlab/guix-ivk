#!/usr/bin/env bb

;; DEPRECATED

(ns notes-create-id
  (:require [cheshire.core :as json]
            [clojure.java.io :as io]
            [clojure.java.shell :as sh]
            [clojure.string :as str]
            [clojure.tools.cli :as cli]))


(def cli-options
  [[nil "--id ID" "Locate the note identified by ID (required)"]
   [nil "--browser BROWSER" "Open the note in BROWSER ('emacshelper' or 'notes')"]
   ["-h" "--help"]])


(assert (System/getenv "IVK_QB_EMACSHELPER"))
(assert (System/getenv "IVK_QB_NOTES"))


(defn qutebrowser-socket [qb-dir]
  (let [runtime-dir (io/file qb-dir "runtime")
        sockets (.listFiles runtime-dir)]
    (->> sockets
         (sort-by #(.lastModified %))
         last
         .getCanonicalPath)))


(defn qutebrowser-send [qb-dir cmd]
  (let [msg {:args [cmd]
             :target_arg nil
             :version "1.8.1"
             :protocol_version 1
             :cwd "."}
        msg-str (str (json/generate-string msg) "\n")
        socket (qutebrowser-socket qb-dir)]
    (sh/sh "socat" "-" (str "UNIX-CONNECT:" socket) :in msg-str)))


(defn open [browser id]
  (assert (#{"emacshelper" "notes"} browser))
  (let [qb-dir (case browser
                 "emacshelper" (System/getenv "IVK_QB_EMACSHELPER")
                 "notes" (System/getenv "IVK_QB_NOTES"))
        cmd (str ":open http://localhost:50057/note/" id)]
    (qutebrowser-send qb-dir cmd)))


(defn main [args]
  (let [{:keys [options summary errors]} (cli/parse-opts args cli-options)
        {:keys [help id browser]} options]
    (cond help (println summary)
          errors (binding [*out* *err*] (run! println errors))
          (and id browser) (open browser id)
          :else (do (binding [*out* *err*] (println "Not all required keys are used!"))
                    (println summary)))))


(main *command-line-args*)
nil
