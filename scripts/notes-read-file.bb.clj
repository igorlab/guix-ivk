#!/usr/bin/env bb

;; DEPRECATED

(ns notes-create-id
  (:require [babashka.curl :as curl]
            [cheshire.core :as json]
            [clojure.java.io :as io]
            [clojure.java.shell :as sh]
            [clojure.string :as str]))


(assert (System/getenv "IVK_NOTES_PORT"))
(assert (System/getenv "IVK_NOTIFY_ICON_DIR"))
(assert (System/getenv "IVK_NOTIFY_DURATION"))

(def port (Integer/parseInt (System/getenv "IVK_NOTES_PORT")))
(def address (format "http://localhost:%d" port))


(defn ensure-trailing-slash [dir-path]
  (str dir-path (when-not (str/ends-with? dir-path "/") "/")))

(def icon-dir (ensure-trailing-slash (System/getenv "IVK_NOTIFY_ICON_DIR")))


(defn read-file [path]
  (let [params (json/generate-string {:path path})
        resp (curl/get (str address "/api/read-file")
                       {:body params
                        :headers {"Content-Type" "application/json"}})]
    (-> resp :body)))


(defn report-error [msg]
  (binding [*out* *err*] (println msg))
  (sh/sh "notify-send"
         "-t" (System/getenv "IVK_NOTIFY_DURATION")
         msg
         "-i" (str icon-dir "arctic-fox-simple.png")))


(defn report-success [msg]
  (sh/sh "notify-send"
         "-t" (System/getenv "IVK_NOTIFY_DURATION")
         msg
         "-i" (str icon-dir "knight.png")))


(defn main []
  (let [f (nth *command-line-args* 0)]
    (try
      (read-file f)
      (report-success (str "Loaded: " (-> f io/file .getName)))
      (catch Exception e
        (report-error (str "Oblom: " (-> f io/file .getName)))))))


(main)
nil
