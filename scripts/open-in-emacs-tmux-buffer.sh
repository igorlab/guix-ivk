#!/bin/bash
#
# Capture the current tmux pane and copy its content to Emacs.

rm /tmp/tmux-buffer
tmux capture-pane -S -50000
tmux save-buffer /tmp/tmux-buffer
emacsclient --eval "(ivk/create-temporary-buffer-from-file \"/tmp/tmux-buffer\")" > /dev/null
wmctrl -s 0
