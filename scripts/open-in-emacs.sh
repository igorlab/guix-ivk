#!/bin/bash
#
# Open a file (or multiple files) in Emacs.

for var in "$@"
do
    emacsclient --eval "(find-file \"$var\")" > /dev/null
done
