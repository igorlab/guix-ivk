#!/usr/bin/env bb
(ns copy-configs
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.java.shell :as shell]))


(defn ensure-trailing-slash [dir-path]
  (str dir-path (when-not (str/ends-with? dir-path "/") "/")))


(defn ensure-no-trailing-slash [path]
  (if (str/ends-with? path "/")
    (subs path 0 (dec (count path)))
    path))


(def home (str (System/getenv "HOME") "/"))
(def installation-dir
  (-> "/home/ivk/.guix-profile/share/ivk-configs/"
      io/file
      .getCanonicalPath
      ensure-trailing-slash))


;; Currently, babashka doesn't include lazy-cat.
;; I'll copy it from Clojure directly for now.
(defmacro lazy-cat
  "Expands to code which yields a lazy sequence of the concatenation
  of the supplied colls.  Each coll expr is not evaluated until it is
  needed.

  (lazy-cat xs ys zs) === (concat (lazy-seq xs) (lazy-seq ys) (lazy-seq zs))"
  {:added "1.0"}
  [& colls]
  `(concat ~@(map #(list `lazy-seq %) colls)))


(defn list-files-recursively [dir]
  (let [content (-> dir io/file .listFiles)
        files (filter #(.isFile %) content)
        dirs (filter #(.isDirectory %) content)]
    (lazy-cat files (mapcat list-files-recursively dirs))))


(defn parent [path]
  (let [n (-> path ensure-no-trailing-slash (str/last-index-of "/"))]
    (subs path 0 n)))


(defn config-files-pairs [src-dir]
  (for [f (list-files-recursively src-dir)
        :let [src-path (.getCanonicalPath f)
              dst-path (str/replace src-path src-dir home)]]
    {:src src-path
     :dst dst-path}))


(defn install-configs []
  (doseq [{:keys [src dst]} (config-files-pairs installation-dir)]
    (shell/sh "mkdir" "-p" (parent dst))
    (io/copy (io/file src) (io/file dst))
    (shell/sh "chmod" "+w" dst)))


(defn update-repo-configs [src-dir]
  (doseq [{:keys [src dst]} (config-files-pairs src-dir)
          :when (-> dst io/file .exists)]
    (io/copy (io/file dst) (io/file src))))


(defn check-env []
  (when-not (System/getenv "IVK_GUIX_REPO_DIR")
    (binding [*out* *err*]
      (println "Please set IVK_GUIX_REPO_DIR variable.")
      (System/exit 1))))


(defn main [args]
  (case (first args)
    (nil "--help") (println "Usage: ivk-configs.bb.clj [--install | --update-repo]")
    "--install" (install-configs)
    "--update-repo"
    (do (check-env)
        (update-repo-configs (-> (System/getenv "IVK_GUIX_REPO_DIR")
                                 ensure-trailing-slash
                                 (str "configs"))))))


(main *command-line-args*)
