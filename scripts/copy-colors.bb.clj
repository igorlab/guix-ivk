#!/usr/bin/env bb
(ns notes-create-id
  (:require [clojure.java.shell :as sh]
            [clojure.string :as str]))


(defn to-clipboard [s]
  (sh/sh "xclip" "-selection" "clipboard" :in s))


(->> (System/getenv "QUTE_TEXT")
     slurp
     (re-seq #"#[0-9a-fA-F]{6}")
     (str/join "\n")
     to-clipboard)
