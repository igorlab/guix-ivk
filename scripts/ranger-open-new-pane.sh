#!/bin/bash
#
# Switch to the ranger pane and open a new tab with the current directory.
# (Called from fish.)

tmux select-window -t main:rgr
tmux send-keys -t main:rgr ':' ; sleep 0.005
tmux send-keys -t main:rgr 'tab_new' C-m
tmux send-keys -t main:rgr ':' ; sleep 0.005
tmux send-keys -t main:rgr "cd `pwd`" C-m
