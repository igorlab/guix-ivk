#!/usr/bin/env bb

;; DEPRECATED

(ns notes-create-id
  (:require [babashka.curl :as curl]
            [cheshire.core :as json]
            [clojure.java.shell :as sh]
            [clojure.tools.cli :as cli]))


(def cli-options
  [[nil "--address ADDRESS" "Locate the note pointed by ADDRESS (required)"]
   ["-h" "--help"]])


(assert (System/getenv "IVK_NOTES_PORT"))
(def port (Integer/parseInt (System/getenv "IVK_NOTES_PORT")))
(def address (format "http://localhost:%d" port))


(defn lookup [address-or-id]
  (let [params (json/generate-string {:address address-or-id})
        resp (curl/get (str address "/api/lookup")
                       {:body params
                        :headers {"Accept" "application/json"
                                  "Content-Type" "application/json"}})]
    (-> resp :body (json/parse-string true))))


(defn open-in-emacs [file line]
  (sh/sh "emacsclient" "--eval"
         (format "(progn (find-file \"%s\") (goto-line %d) (recenter-top-bottom 5))"
                 file (inc line))))


(defn goto [origin]
  (let [{:tv5/keys [origin-file origin-line]} origin]
    (open-in-emacs origin-file origin-line)
    (sh/sh "bspc" "desktop" "-f" "Em")))


(defn main [args]
  (let [{:keys [options summary errors]} (cli/parse-opts args cli-options)
        {:keys [help address]} options]
    (cond help (println summary)
          errors (binding [*out* *err*] (run! println errors))
          address (goto (lookup address))
          :else (do (binding [*out* *err*] (println "Not all required keys are used!"))
                    (println summary)))))


(main *command-line-args*)
