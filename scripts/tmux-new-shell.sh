#!/bin/bash

tmux select-window -t 'main:$' \
    ";" new-window -t 'main:+1' \
    ";" switch -t 'main:$' \
    ";" send-keys "ls -alh" C-m

wmctrl -s 2
