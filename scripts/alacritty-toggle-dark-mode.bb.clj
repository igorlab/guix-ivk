#!/usr/bin/env bb
(ns tmux
  (:require [clojure.string :as str]))

(def config-path (str (System/getenv "HOME") "/.config/alacritty/alacritty.yml"))

(defn toggle-dark-mode! [path]
  (let [s (slurp path)
        dark (str/replace s "colors: *light" "colors: *dark")
        light (str/replace s "colors: *dark" "colors: *light")
        toggled (if (= s dark) light dark)]
    (spit path toggled)))


(do
  (toggle-dark-mode! config-path)
  nil)
