#!/bin/sh
#
# Move node to the Misc-I desktop (or Misc-II, if the current desktop is Misc-I).

current_desktop=$(bspc query --desktops -d --names)
if [ "$current_desktop" = "Misc-I" ]; then
    next="Misc-II"
else
    next="Misc-I"
fi

bspc node focused --to-desktop "$next"
