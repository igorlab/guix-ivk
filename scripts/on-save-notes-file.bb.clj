#!/usr/bin/env bb

;; DEPRECATED

(ns switch-background
  (:require [babashka.curl :as curl]
            [cheshire.core :as json]
            [clojure.java.io :as io]
            [clojure.java.shell :as sh]))


(assert (System/getenv "IVK_NOTES_PORT"))
(assert (System/getenv "IVK_QB_EMACSHELPER"))

(def port (Integer/parseInt (System/getenv "IVK_NOTES_PORT")))
(def address (format "http://localhost:%d/api/read-piece" port))

(def params (json/generate-string
             {:path (nth *command-line-args* 0)
              :line (Integer/parseInt (nth *command-line-args* 1))}))


(defn qutebrowser-socket [qb-dir]
  (let [runtime-dir (io/file qb-dir "runtime")
        sockets (.listFiles runtime-dir)]
    (->> sockets
         (sort-by #(.lastModified %))
         last
         .getCanonicalPath)))


(defn qutebrowser-send [qb-dir cmd]
  (let [msg {:args [cmd]
             :target_arg nil
             :version "1.8.1"
             :protocol_version 1
             :cwd "."}
        msg-str (str (json/generate-string msg) "\n")
        socket (qutebrowser-socket qb-dir)]
    (sh/sh "socat" "-" (str "UNIX-CONNECT:" socket) :in msg-str)))


(def resp
  (-> (curl/post address {:body params
                          :headers {"Accept" "application/json"
                                    "Content-Type" "application/json"}})
      :body
      (json/parse-string true)))


(qutebrowser-send (System/getenv "IVK_QB_EMACSHELPER")
                  (format ":open http://localhost:%d/note/%s" port (:id resp)))
