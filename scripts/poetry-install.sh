#!/bin/bash
#
# Install a python library in a global python environment
# (when a local environment is activated).

rm -rf dist
poetry build
f=$(find dist | grep whl)
f=$(realpath "$f")

echo
echo PAY ATTENTION: this should point to the main python
bash -c "source .venv/bin/activate && deactivate && which python"
echo

bash -c "source .venv/bin/activate && deactivate && pip uninstall -y $f && pip install $f"
