#!/usr/bin/env bb
(ns switch-background
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.java.shell :as sh]
            [clojure.tools.cli :as cli]))


(def random-seed-file (io/file (System/getenv "HOME") "tmp/seed"))


(defn maybe-slurp [f]
  (when (.exists (io/file f))
    (slurp f)))


(def cli-options
  [[nil "--dir DIR" "Look for wallpapers in DIR"]
   [nil "--root DIR" "Ask for choice (from DIR subdirectories)"]
   [nil "--dry-run" "Don't call feh --bg-tile"]
   ["-h" "--help"]])


(def picture-extensions #{".jpg" ".jpeg" ".png"})


(defn extension [file]
  (let [name (-> file io/file .getName)
        parts (str/split name #"\.")]
    (str "." (last parts))))


(defn picture? [file]
  (boolean (picture-extensions (extension file))))


(defn list-pictures [dir]
  (->> dir
       io/file
       .listFiles
       (filter picture?)))


(defn list-directories [dir]
  (->> dir
       io/file
       .listFiles
       (filter #(.isDirectory %))))


(defn day-of-year' []
  (let [now (java.time.ZonedDateTime/now)
        day (.getDayOfYear now)]
    (if (>= (.getHour now) 4)
      day
      (dec day))))


(defn picture-of-the-day [dir & [seed]]
  (when-let [pics (->> (list-pictures dir)
                       (sort-by #(.hashCode (str seed %)))
                       seq)]
    (let [index (mod (day-of-year') (count pics))]
      (.getCanonicalPath (nth pics index)))))


(defn set-background [path]
  (sh/sh "feh" "--bg-tile" path))


(defn ask-choice [lines]
  (let [choices-file "/dev/shm/fzf-tmp-file"
        result-file "/dev/shm/fzf-tmp-result"
        fzf-command (format "cat '%s' | fzf > '%s'" choices-file result-file)
        _ (spit choices-file (str/join "\n" lines))
        _ (sh/sh "urxvt"
                 "-name" "URxvtChoices"
                 "-e" "bash" "-c" fzf-command)
        result (-> result-file slurp str/trim)]
    (when-not (empty? result)
      result)))


(defn wallpaper-dir-choices [wallpapers-root]
  (->> wallpapers-root
       list-directories
       (map #(.getName %))
       (remove #(str/starts-with? % "_"))
       sort))


(defn pictures-dir [root dir]
  (assert (or dir root (System/getenv "IVK_WALLPAPERS_DIR")))
  (let [root (if (and (not dir) (not root)) (System/getenv "IVK_WALLPAPERS_DIR") root)]
    (if dir
      dir
      (when-let [choice (ask-choice (wallpaper-dir-choices root))]
          (io/file root choice)))))


(defn main [args]
  (let [{:keys [options summary errors]} (cli/parse-opts args cli-options)
        {:keys [help dir root dry-run]} options
        seed (maybe-slurp random-seed-file)]
    (cond help (println summary)
          errors (binding [*out* *err*] (run! println errors))
          :else (when-let [pic (some-> (pictures-dir root dir) (picture-of-the-day seed))]
                  (when-not dry-run
                    (set-background pic))
                  (println pic)))))


(main *command-line-args*)
