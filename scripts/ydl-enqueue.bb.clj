#!/usr/bin/env bb
(ns ydl-enqueue
  (:require [clojure.string :as str]
            [clojure.pprint :refer [pprint]]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.java.shell :as shell]))


(assert (System/getenv "IVK_YDL_DOWNLOAD_DIR"))
(assert (System/getenv "IVK_NOTIFY_ICON_DIR"))
(assert (System/getenv "IVK_NOTIFY_DURATION"))


(defn ensure-trailing-slash [dir-path]
  (str dir-path (when-not (str/ends-with? dir-path "/") "/")))


(def download-dir (ensure-trailing-slash (System/getenv "IVK_YDL_DOWNLOAD_DIR")))
(def queues-dir (str download-dir "-queues/"))
(def icon-dir (ensure-trailing-slash (System/getenv "IVK_NOTIFY_ICON_DIR")))


(defn tmux-window-exists? [session window]
  (->
   (shell/sh "tmux" "has-session" "-t" (str session ":" window))
   :exit
   (= 0)))


(defn read-clipboard []
  (:out
   (shell/sh "xclip" "-o" "-selection" "clipboard")))


(defn report-error [msg]
  (binding [*out* *err*] (println msg))
  (shell/sh "notify-send"
            "-t" (System/getenv "IVK_NOTIFY_DURATION")
            msg
            "-i" (str icon-dir "arctic-fox-simple.png")))


(defn report-success [msg]
  (shell/sh "notify-send"
            "-t" (System/getenv "IVK_NOTIFY_DURATION")
            msg
            "-i" (str icon-dir "video-download.png")))


(defn current-time-str []
  (.format
   (java.time.ZonedDateTime/now)
   (java.time.format.DateTimeFormatter/ofPattern "yyyy-MM-dd_kk:mm:ss")))


(defn read-queue [path]
  (-> path slurp edn/read-string))


(defn write-queue [path queue]
  (spit path (with-out-str (pprint queue))))


(defn create-queue [queues-dir]
  (spit (str queues-dir (current-time-str) ".edn")
        "#{}"))


(defn last-queue [queues-dir]
  (->> (io/file queues-dir)
       .listFiles
       sort
       last))


(defn ensure-queue [queues-dir]
  (when-not (last-queue queues-dir)
    (create-queue queues-dir)))


(defn enqueue [queues-dir url]
  (let [now (java.util.Date.)
        last-queue-file (last-queue queues-dir)
        q (read-queue last-queue-file)
        video (->> q (filter #(= (:url %) url)) first)
        updated-q (if video
                    (-> q
                        (disj video)
                        (conj (assoc video :bumped now)))
                    (-> q
                        (conj {:url url :added now})))]
    (write-queue last-queue-file updated-q)))


(defn sort-queue [q]
  (concat
   (->> q
        (filter :bumped)
        (sort-by :bumped)
        reverse)
   (->> q
        (remove :bumped)
        (sort-by :added))))


(defn ydl-script [queue-file]
  (->> (read-queue queue-file)
       sort-queue
       (map #(format "yt-dlp \"%s\"" (:url %)))
       (str/join "\n")
       (format "#!/bin/bash\n\n%s\n")))


(defn directory-for-queue [queues-dir queue-file]
  (format "%s../%s/"
          queues-dir
          (-> queue-file
              io/file
              .getName
              (str/replace #"\.edn$" ""))))


(defn download-last-queue [queues-dir]
  (let [queue-file (last-queue queues-dir)
        script-dir (directory-for-queue queues-dir queue-file)
        script-path (str script-dir "down.bash")
        script-content (ydl-script queue-file)]
    (shell/sh "mkdir" script-dir)
    (spit script-path script-content)
    (shell/sh "chmod" "+x" script-path)
    (shell/sh "tmux" "new-window"
              "-t" "services"
              "-n" "ydl"
              "-c" script-dir
              "bash -c ./down.bash")
    (report-success (format "Downloading %d items..." (-> queue-file read-queue count)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main

(defn try-enqueue [queues-dir url]
  (if (and url (str/starts-with? url "http"))
    (do
      (when (empty? (read-queue (last-queue queues-dir)))
        (create-queue queues-dir))
      (enqueue queues-dir url)
      (report-success (format "Enqueued %s" url)))
    (report-error (format "Not a URL: '%s'" url))))


(defn print-ydl-script [queue-file]
  (println (ydl-script queue-file)))


(defn ensure-no-downloads []
  (if (tmux-window-exists? "services" "ydl")
    (do
      (report-error "ydl is already running!")
      false)
    true))


(defn ensure-last-queue-not-empty [queues-dir]
  (let [q (read-queue (last-queue queues-dir))]
    (when (empty? q)
      (report-error "Nothing to download!"))
    (seq q)))


(defn download-with-tmux [queues-dir]
  (and
   (ensure-no-downloads)
   (ensure-last-queue-not-empty queues-dir)
   (download-last-queue queues-dir)
   (create-queue queues-dir)))


(defn main [args]
  (ensure-queue queues-dir)
  (case (first args)
    "--clear-queue" (spit (last-queue queues-dir) "#{}")
    ("--xclip" nil) (try-enqueue queues-dir (read-clipboard))
    "--print-ydl-script" (print-ydl-script (or (second args) (last-queue queues-dir)))
    "--download" (download-with-tmux queues-dir)
    (try-enqueue queues-dir (first args))))


(main *command-line-args*)
