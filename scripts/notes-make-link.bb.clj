#!/usr/bin/env bb

;; I don't really use this thing...

(ns notes-create-id
  (:require [clojure.string :as str]))


(defn main [[address title fmt]]
  (let [[_ id] (re-find #".*/([0-9a-zA-Z.-]+)" address)
        title (str/replace title "\"" "\\\"")
        result (case fmt
                 "id" id
                 "short" (format "{r/%s}" id)
                 "textless" (format "{r/%s \"\"}" id)
                 "full" (format "{r/%s \"%s\"}" id title))]
    (print result)))


(assert (= (count *command-line-args*) 3))
(main *command-line-args*)
