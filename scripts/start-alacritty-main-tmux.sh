#!/bin/bash
#
# Start alacritty with tmux; attach to 'main' tmux session.

alacritty --working-directory "/home/ivk" -e tmux attach -t main &
