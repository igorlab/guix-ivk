#!/bin/bash
#
# Shutdown the computer in X minutes.
# FIXME: hard-coded paths.

shutdown --no-wall -c

choices='1\n2\n3\n5\n10\n15\n20\n30\n60\ncancel'

urxvt -name URxvtChoices \
      -geometry 20x13 \
      -e fish -c "printf $choices | fzf --tac --no-sort --bind ctrl-x:print-query > /tmp/_dosvidos_val"

dosvidos_time=$(</tmp/_dosvidos_val)

if [ -z $dosvidos_time ]; then
    exit 1
fi

shutdown --no-wall -h $dosvidos_time
if [ $? -eq 0 ]; then
    python ~/dev/cfg/all-sorts-of-stuff/py-ivk/ivkscripts/change-wallpaper.py /storage/p/wall/dosvidos
else
    clock.sh
    event-track.sh
    python ~/dev/cfg/all-sorts-of-stuff/py-ivk/ivkscripts/change-wallpaper.py /storage/p/wall/itm
    exit 1
fi

prep_time=$(echo "$dosvidos_time - 0.5" | bc)"m"
sleep $prep_time
prepare-shutdown.sh
