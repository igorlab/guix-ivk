#!/bin/sh
#
# For desktops with feh-induced wallpapers:
#   Switch to next node, discregarding wallpaper-like nodes.
#
# For Term:
#   Switch from shell to lf and back.

direction=$1
desktop=$(bspc query -D -d focused --names)


with_custom_wallpapers() {
    for counter in `seq 1 10`; do
        bspc node -f $direction.local.!hidden.window
        active_window=$(xprop -root _NET_ACTIVE_WINDOW | cut -d ' ' -f 5)
        classname=$(xprop -id $active_window WM_CLASS | awk '{print $4}')
        case "$classname" in
        '"feh/notes"' | '"feh/reader"' | '"feh/misc-i"' | '"feh/misc-ii"')
            continue
            ;;
        esac
        break
    done
}


with_tmux() {
    bspc node -f $direction.local.!hidden.window # just in case
    session=$(tmux list-panes -t "$TMUX_PANE" -F '#S' | head -n1)
    case $session in
    lf)
        tmux switch -t main ;;
    *)
        tmux switch -t lf
    esac
}


with_emacs() {
    monocle=$(bspc query -T -d | grep monocle)
    if [ -n "$monocle" ]; then
        emacsclient --eval "(ivk.clojure/switch-to-or-out-of-repl-window)"
    else
        bspc node -f $direction.local.!hidden.window
    fi
}


case "$desktop" in
Notes | Reader | Misc-I | Misc-II)
    with_custom_wallpapers
    ;;
# Em)
#     with_emacs
#     ;;
Term)
    with_tmux
    ;;
*)
    bspc node -f $direction.local.!hidden.window
esac
