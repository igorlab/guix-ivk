(define-module (guix-ivk packages lazygit)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix download)
  #:use-module (guix build-system trivial)

  #:use-module (guix utils)
  #:use-module (guix build utils)

  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)

  #:use-module (gnu packages))


(define-public ivk-lazygit
  (package
    (name "ivk-lazygit")
    (version "0.29")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append "https://github.com/jesseduffield/lazygit/releases/download/v"
                       version
                       "/lazygit_0.29_Linux_x86_64.tar.gz"))
       (sha256
        (base32
         "0hp0gpi2ndgkgzq9rayrmv5w2310a5arsfxw3vq9dhb3sgz8r7x5"))))
    (build-system trivial-build-system)
    (native-inputs `(("tar" ,tar)
                     ("gzip" ,gzip)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils))
                   (let* ((tar    (assoc-ref %build-inputs "tar"))
                          (gz     (assoc-ref %build-inputs "gzip"))
                          (source (assoc-ref %build-inputs "source"))
                          (out    (assoc-ref %outputs "out"))
                          (bin   (string-append out "/bin")))
                     (copy-file source "./source-lazygit.tar.gz")
                     (invoke (string-append gz "/bin/gzip")
                             "-d"
                             "source-lazygit.tar.gz")
                     (invoke (string-append tar "/bin/tar")
                             "xf"
                             "source-lazygit.tar")
                     (mkdir-p bin)
                     (copy-file "lazygit"
                                (string-append bin "/lazygit"))))))
    (synopsis "lazygit")
    (description
     "lazygit")
    (home-page "https://github.com/jesseduffield/lazygit")
    (license bsd-3) ; but actually, it's MIT
    ))

