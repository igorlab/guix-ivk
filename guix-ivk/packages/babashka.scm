(define-module (guix-ivk packages babashka)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix licenses/)
  #:use-module (guix download)
  #:use-module (guix build-system trivial)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages))


(define-public ivk-babashka
  (package
    (name "ivk-babashka")
    (version "0.6.2")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append "https://github.com/borkdude/babashka/releases/download/v"
                       version
                       "/babashka-"
                       version
                       "-linux-amd64.tar.gz"))
       (sha256
        (base32
         "1inykwismfnpkdrkg61pdgpgk037w31ivw47m53clb0r3p3x1qj0"))))
    (build-system trivial-build-system)
    (native-inputs `(("tar" ,tar)
                     ("gzip" ,gzip)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils))
                   (let* ((tar    (assoc-ref %build-inputs "tar"))
                          (gz     (assoc-ref %build-inputs "gzip"))
                          (source (assoc-ref %build-inputs "source"))
                          (out    (assoc-ref %outputs "out"))
                          (bin   (string-append out "/bin")))
                     (copy-file source "./babashka-source.tar.gz")
                     (invoke (string-append gz "/bin/gzip")
                             "-d"
                             "babashka-source.tar.gz")
                     (invoke (string-append tar "/bin/tar")
                             "xf"
                             "babashka-source.tar")
                     (mkdir-p bin)
                     (copy-file "bb"
                                (string-append bin "/bb"))))))
    (synopsis "babashka")
    (description
     "babashka")
    (home-page "https://github.com/borkdude/babashka")
    (license licenses/epl1.0)))
