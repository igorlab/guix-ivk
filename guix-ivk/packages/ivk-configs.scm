(define-module (guix-ivk packages ivk-configs)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module (guix utils)
  #:use-module ((guix-ivk version) #:prefix ivk/))

(define-public ivk-configs
  (package
    (name "ivk-configs")
    (version ivk/version)
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/igorlab/guix-ivk.git")
             (commit version)))
       (sha256 (base32 ivk/hash))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       '(("configs" "share/ivk-configs"))
       #:phases
       (modify-phases %standard-phases
         (delete 'patch-source-shebangs)
         (delete 'patch-generated-file-shebangs)
         (delete 'patch-shebangs))))
    (synopsis "My configs.")
    (description
     "My configs.")
    (home-page "https://gitlab.com/igorlab/")
    (license wtfpl2)))
