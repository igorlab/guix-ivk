(define-module (guix-ivk packages symbola)
  #:use-module (ice-9 regex)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system font)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages xorg))

(define-public ivk-font-symbola
  (package
    (name "ivk-font-symbola")
    (version "13.00-7")
    (source (origin
              (method url-fetch)
              (uri "https://gitlab.com/igorlab/guix-ivk/-/raw/master/tmp/symbola-repackaged-3.zip")
              (sha256
               (base32
                "0r00m9ssn58mi1kd8jvqprh1d5g5pg5hd7bmdqc6vln7qr2xw40h"))))
    (build-system font-build-system)
    (home-page "https://dn-works.com/ufas/")
    (synopsis "Symbola")
    (description "Symbola")
    (license license:silofl1.1) ; Not really - it has its own custom license.
))
