(define-module (guix-ivk packages tty-clock)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages ncurses))

(define-public ivk-tty-clock
  (let ((commit "9b2c86d904efc8e0248a10c2842b5103924333aa")
        (revision "1"))
    (package
      (name "ivk-tty-clock")
      (version (git-version "2.3" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/xorg62/tty-clock.git")
               (commit commit)))
         (sha256
          (base32
           "0w8qx7qrvr1as7f2mb617n82qpi90c01swvj1yijbgqvmaa8ycfd"))))
      (build-system gnu-build-system)
      (arguments
       `(#:tests? #f
         #:phases (modify-phases %standard-phases (delete 'configure))
         #:make-flags (list (string-append "CC=" ,(cc-for-target))
                            (string-append "PREFIX=" %output))))
      (native-inputs
       `(("pkg-config" ,pkg-config)
         ("ncurses" ,ncurses)))
      (synopsis "tty-clock")
      (description
       "tty-clock")
      (home-page "https://github.com/xorg62/tty-clock")
      (license bsd-3))))

